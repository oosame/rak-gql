const defaultTheme = require("tailwindcss/defaultTheme");

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [],
  theme: {
    extend: {
      fontFamily: {
        sans: ["Poppins", "Noto Kufi Arabic", ...defaultTheme.fontFamily.sans],
      },
      colors: {
        theme: {
          base: "#FCFCFC",
          text: "#000000",
          "dark-red": "#3E0E11",
        },
      },
    },
  },
  plugins: [require("@headlessui/tailwindcss")({ prefix: "ui" })],
};
