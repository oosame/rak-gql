export default defineI18nConfig(() => ({
  legacy: false,
  locale: "en",
  availableLocales: ["en", "ar"],
  fallbackLocale: "en",
}));
