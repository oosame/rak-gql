// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: false },
  modules: [
    "@nuxtjs/tailwindcss",
    "@nuxtjs/i18n-edge",
    "@nuxtjs/google-fonts",
    "nuxt-headlessui",
    "@nuxtjs/apollo",
    "nuxt-swiper",
  ],
  app: {
    head: {
      link: [{ rel: "icon", type: "image/svg", href: "/favicon.svg" }],
    },
  },
  i18n: {
    defaultLocale: "en",
    locales: [
      {
        code: "ar",
        iso: "ar",
        name: "العربية",
        dir: "rtl",
        file: "ar.json",
      },
      {
        code: "en",
        iso: "en",
        name: "English",
        dir: "ltr",
        file: "en.json",
      },
    ],
    strategy: "prefix",
    langDir: "i18n",
    vueI18n: "i18n.config.ts",
  },
  googleFonts: {
    families: {
      Poppins: [400, 500, 600, 700, 800, 900],
      "Noto+Kufi+Arabic": [400, 500, 600, 700, 800, 900],
    },
  },
  headlessui: {
    prefix: "",
  },
  apollo: {
    clients: {
      default: {
        httpEndpoint: "https://heartofrak.com/wp/graphql",
      },
    },
  },
  ssr: false,
});
